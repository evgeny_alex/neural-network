import program.cCell as cC

class CLayer:
    def __init__(self, size, num_layer):
        self.size = size
        self.planes = []
        self.array_CCell = []
        self.num_layer = num_layer

    def compute_outputs(self, array_SCell):
        for sCell in array_SCell:
            cCell = cC.CCell(self.size, self.num_layer)
            cCell.compute_output(sCell)
            self.array_CCell.append(cCell)

    def get_number(self):
        out = []
        for cCell in self.array_CCell:
            out.append(cCell.output)

        number = out.index(max(out))
        return number % 10