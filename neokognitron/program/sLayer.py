import program.sCell as sc
import math

radius_recp = 3     # radius receptive cell
q = 0.5             # speed learnig

def wavelet(x):
    return (x*x - 1)*math.exp(-x*x/2)

def get_init_weights(picture, output):
    weights = []

    for i in range(len(output)):
        col = i % 28
        row = int(i / 28)
        a = []
        for j in range(-radius_recp, radius_recp + 1): # rows
            for k in range(-radius_recp, radius_recp + 1): # columns
                if col + k < 0 or col + k > 27 or row + j < 0 or row + j > 27: # Out the scope cell
                    a.append(0)
                else:
                    out_picture = picture[(row + j) * 28 + col + k]
                    a.append(q * wavelet(out_picture) * out_picture)
        weights.append(a)

    return weights

def get_weights(matrix, size):
    weights = []

    for i in range(size*size):
        weights.append(matrix)

    return weights

class SLayer:
    def __init__(self, radius, size_weights):
        self.array_features = []
        self.features = []
        self.array_SCell = []
        self.last_layer_features = []
        self.last_SCell = []
        self.radius = radius
        self.size_weights = size_weights
        for i in range(10):
            self.last_layer_features.append([])
            self.last_SCell.append(sc.SCell([], [], [0] * 3 * 3, 3, 1, 3))

    def has_feature(self, feature):
        for i in range(len(self.features)):
            if self.features[i] == feature:
                return True
        return False

    def add_feature(self, feature, picture, size):
        output = [0]*size*size # Count neurons in cell
        # weights = get_init_weights(picture, output)
        weights = get_weights(feature.matrix, 28)
        sCell = sc.SCell(feature, weights, output, size, self.radius, self.size_weights)
        self.array_SCell.append(sCell)
        self.features.append(feature)

    def learning_cells(self, picture):
        i = 0
        for cell in self.array_SCell:
            i += 1
            print("learning " + str(i) + " cell of " + str(len(self.array_SCell)))
            cell.change_weights(picture)

    def learning_inner_cells(self, array_prev_CCells):
        i = 0
        for cell in self.array_SCell:
            i += 1
            print("learning " + str(i) + " cell of " + str(len(self.array_SCell)))
            cell.change_inner_weights(array_prev_CCells)

    def learning_last_cells(self, array_prev_CCells):
        i = 0
        for cell in self.last_SCell:
            i += 1
            print("learning " + str(i) + " cell of " + str(len(self.last_SCell)))
            cell.change_inner_weights(array_prev_CCells)


    def add_inner_feature(self, feature, size):
        output = [0] * size * size  # Count neurons in cell
        weights = get_weights(feature.matrix, size)
        sCell = sc.SCell(feature, weights, output, size, self.radius, self.size_weights)
        self.array_SCell.append(sCell)

        self.features.append(feature)
        for i in range(len(feature.matrix)):
            self.array_features.append(feature.matrix[i])

    def add_last_feature(self, feature, size):
        output = [0] * size * size  # Count neurons in cell

        digit = feature.digit
        for i in range(len(feature.matrix)):
            mylist = self.last_layer_features.pop(digit)
            mylist.append(feature.matrix[i])

            sCell = self.last_SCell.pop(digit)
            sCell.weights.append(mylist)

            self.last_SCell.insert(digit, sCell)
            self.last_layer_features.insert(digit, mylist)