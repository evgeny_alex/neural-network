import math

def wavelet(x):
    return (x*x - 1)*math.exp(-x*x/2)

def psi(x):
    maximum = max(x, 0)
    return maximum/(1 + maximum)

def fi(x):
    if x > 100:
        return 1
    if x < -50:
        return 0
    return 1 / (1 + math.exp((-1) * 0.5 * x))

def conv_matrix28to14(matrix_28):
    matrix_14 = []
    sum_value = 0

    centre_pulling = []
    for i in range(28 * 28):
        if i % 2 == 0 and (int(i / 28)) % 2 == 0:
            centre_pulling.append(i)

    for j in centre_pulling:
        for row in range(2):
            for col in range(2):
                sum_value += wavelet(matrix_28[j + row * 27 + col]) * matrix_28[j + row * 27 + col]
                # sum_value += matrix_28[j + row * 27 + col]
        # matrix_14.append(psi(sum_value))
        matrix_14.append(fi(sum_value))
        sum_value = 0

    return matrix_14

def conv_matrix14to7(matrix_14):
    matrix_7 = []
    sum_value = 0

    centre_pulling = []
    for i in range(14 * 14):
        if i % 2 == 0 and (int(i / 14)) % 2 == 0:
            centre_pulling.append(i)

    for j in centre_pulling:
        for row in range(2):
            for col in range(2):
                sum_value += wavelet(matrix_14[j + row * 13 + col]) * matrix_14[j + row * 13 + col]
                # sum_value += matrix_28[j + row * 27 + col]
        # matrix_14.append(psi(sum_value))
        matrix_7.append(fi(sum_value))
        sum_value = 0

    return matrix_7

def conv_matrix7to3(matrix_7):
    matrix_3 = []
    sum_value = 0

    centre_pulling = [8, 10, 12,
                      22, 24, 26,
                      36, 38, 40]

    for j in centre_pulling:
        for row in range(-1, 2):
            for col in range(-1, 2):
                sum_value += wavelet(matrix_7[j + row * 6 + col]) * matrix_7[j + row * 6 + col]
                # sum_value += matrix_28[j + row * 27 + col]
        # matrix_14.append(psi(sum_value))
        matrix_3.append(fi(sum_value))
        sum_value = 0

    return matrix_3

def conv_matrix3to1(matrix_3):
    out = 0

    for row in range(3):
        for col in range(3):
            out += wavelet(matrix_3[row * 2 + col]) * matrix_3[row * 2 + col]

    return fi(out)


class CCell:
    def __init__(self, size, number_layer):
        self.size = size        # Size of side cell
        self.output = []
        self.num_layer = number_layer

    def compute_output(self, sCell):
        if self.num_layer == 1:
            self.output = conv_matrix28to14(sCell.output_neurons)
        if self.num_layer == 2:
            self.output = conv_matrix14to7(sCell.output_neurons)
        if self.num_layer == 3:
            self.output = conv_matrix7to3(sCell.output_neurons)
        if self.num_layer == 4:
            self.output = conv_matrix3to1(sCell.output_neurons)