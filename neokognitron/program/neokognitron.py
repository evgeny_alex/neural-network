import program.sLayer as sL
import program.cLayer as cL
import program.vLayer as vL
import program.features as ft
import numpy as np
import matplotlib.pyplot as plt

def plot_image7(array):
    pixels = np.array(array)
    plt.imshow(pixels.reshape((7, 7)), cmap='gray')
    plt.show()

def plot_image4(array_matrix_7):

    for i in range(len(array_matrix_7)):

        matrix_7 = array_matrix_7[i]
        matrix_4 = []
        max_value = 0
        centre_pulling = [8, 9, 11, 12,
                          15, 16, 18, 19,
                          29, 30, 32, 33,
                          36, 37, 39, 40]

        for j in centre_pulling:
            for row in range(-1, 2):
                for col in range(-1, 2):
                    if matrix_7[j + row * 7 + col] > max_value:
                        max_value = matrix_7[j + row * 7 + col]
            matrix_4.append(max_value)
            max_value = 0

        pixels = np.array(matrix_4)
        plt.imshow(pixels.reshape((4, 4)), cmap='gray')
        plt.show()



def fill_area(picture, num_area):
    result = []
    start_col = int(num_area % 4) * 7
    end_col = start_col + 7
    start_row = int(num_area / 4) * 7
    end_row = start_row + 7

    for i in range(start_row, end_row):
        for j in range(start_col, end_col):
            result.append(picture[i * 28 + j])

    return result

def check_on_null(matrix):
    for i in range(len(matrix)):
        if matrix[i] != 0.0 or matrix[i] != -0.0 or matrix[i] != 0:
            return False
    return True

def conv_matrix_7to4(array_matrix_7):
    array_matrix_4 = []

    for i in range(len(array_matrix_7)):

        matrix_7 = array_matrix_7[i]
        matrix_4 = []
        max_value = 0
        centre_pulling = [8, 9, 11, 12,
                          15, 16, 18, 19,
                          29, 30, 32, 33,
                          36, 37, 39, 40]

        for j in centre_pulling:
            for row in range(-1, 2):
                for col in range(-1, 2):
                    if matrix_7[j + row * 7 + col] > max_value:
                        max_value = matrix_7[j + row * 7 + col]
            matrix_4.append(max_value)
            max_value = 0



        array_matrix_4.append(matrix_4)

    return array_matrix_4

def conv_matrix_4to3(array_matrix_4):
    array_matrix_3 = []

    for i in range(len(array_matrix_4)):

        matrix_4 = array_matrix_4[i]
        matrix_3 = []
        max_value = 0
        centre_pulling = [0, 1, 2,
                          4, 5, 6,
                          8, 9, 10]

        for j in centre_pulling:
            for row in range(2):
                for col in range(2):
                    if matrix_4[j + row * 4 + col] > max_value:
                        max_value = matrix_4[j + row * 4 + col]
            matrix_3.append(max_value)
            max_value = 0

        array_matrix_3.append(matrix_3)

    return array_matrix_3


class Neokognitron:

    def __init__(self):
        self.COUNT_TRAIN_PICTURE = 7
        self.COUNT_TEST_PICTURE = 25
        self.U0 = np.array([0.0] * 28 * 28)

        # Layers
        self.S1_layer = sL.SLayer(3, 7)
        self.V1_layer = vL.VLayer()
        self.C1_layer = cL.CLayer(14, 1) # Size of side cell and number of layer

        self.S2_layer = sL.SLayer(2, 4)
        self.V2_layer = vL.VLayer()
        self.C2_layer = cL.CLayer(7, 2)

        self.S3_layer = sL.SLayer(1, 3)
        self.V3_layer = vL.VLayer()
        self.C3_layer = cL.CLayer(3, 3)

        self.S4_layer = sL.SLayer(1, 3)
        self.V4_layer = vL.VLayer()
        self.C4_layer = cL.CLayer(1, 4)

    def train(self, X_train, y_train):

        count_correct = 0
        self.add_S1_features(X_train, y_train)  # Adding features for each layer
        self.add_S2_features()
        self.add_S3_features()
        self.add_S4_features()

        for i in range(self.COUNT_TRAIN_PICTURE):
            print('Picture №' + str(i))
            print('Expected number - ' + str(y_train[i].argmax()))

            self.train_S1(X_train[i])
            self.train_S2()
            self.train_S3()
            self.train_S4()
            number = self.C4_layer.get_number()
            print('Expected number - ' + str(y_train[i].argmax()))
            print('Output number - ' + str(number))
            if number == y_train[i].argmax():
                count_correct += 1

        print("Testing accuracy = " + str(count_correct/self.COUNT_TRAIN_PICTURE * 100) + " %")

    def add_S1_features(self, X_train, y_train):
        first_seven = True

        for i in range(self.COUNT_TRAIN_PICTURE):  # For each train picture, setting features and init weight
            print('Adding features of ' + str(i) + ' pictures. S1 layer')

            picture = X_train[i]
            digit = np.argmax(y_train[i])

            for j in range(16):
                matrix_area = fill_area(picture, j)


                if check_on_null(matrix_area):  # Did not add null feature
                    continue

                # if digit == 7 and first_seven:
                #     plot_image7(matrix_area)

                feature = ft.Feature(1, digit, matrix_area)

                if not self.S1_layer.has_feature(feature):  # If feature is not in S1 layer -> add
                    self.S1_layer.add_feature(feature, picture, 28)

            if digit == 7:
                first_seven = False

        print("Count features in S1 layer = " + str(len(self.S1_layer.features)))

    def add_S2_features(self):
        print('Adding features for S2 layer')
        array_matrix_features = []  # Array of set features
        current_digit = -1
        max_comb_attrs = 4
        comb_attrs = 0
        for j in range(len(self.S1_layer.features)):

            if current_digit == -1:
                current_digit = self.S1_layer.features[j].digit
                array_matrix_features.append(self.S1_layer.features[j].matrix)
                comb_attrs += 1

            elif comb_attrs < max_comb_attrs and current_digit == self.S1_layer.features[j].digit:
                array_matrix_features.append(self.S1_layer.features[j].matrix)
                comb_attrs += 1

            else:
                comb_attrs = 0
                # if current_digit == 7:
                #     plot_image4(array_matrix_features)
                array_matrix_features = conv_matrix_7to4(array_matrix_features)  # Convolution matrix 7*7 to 4*4
                feature = ft.Feature(2, current_digit, array_matrix_features)
                self.S2_layer.add_inner_feature(feature, 14)
                current_digit = -1
                array_matrix_features = []

        if array_matrix_features:  # If not empty array
            feature = ft.Feature(2, current_digit, array_matrix_features)
            self.S2_layer.add_inner_feature(feature, 14)

        print("Count features in S2 layer = " + str(len(self.S2_layer.features)))

    def add_S3_features(self):
        print('Adding features for S3 layer')
        array_matrix_features = []  # Array of set features
        current_digit = -1
        max_comb_attrs = 4
        comb_attrs = 0
        for j in range(len(self.S2_layer.features)):

            if current_digit == -1:
                current_digit = self.S2_layer.features[j].digit
                for feature_matrix in self.S2_layer.features[j].matrix:
                    array_matrix_features.append(feature_matrix)
                comb_attrs += 1

            elif comb_attrs < max_comb_attrs and current_digit == self.S2_layer.features[j].digit:
                for feature_matrix in self.S2_layer.features[j].matrix:
                    array_matrix_features.append(feature_matrix)
                comb_attrs += 1

            else:
                comb_attrs = 0
                array_matrix_features = conv_matrix_4to3(array_matrix_features)  # Convolution matrix 4*4 to 3*3
                feature = ft.Feature(3, current_digit, array_matrix_features)
                self.S3_layer.add_inner_feature(feature, 7)
                current_digit = self.S2_layer.features[j].digit
                array_matrix_features = []
                for feature_matrix in self.S2_layer.features[j].matrix:
                    array_matrix_features.append(feature_matrix)
                comb_attrs += 1

        print("Count features in S3 layer = " + str(len(self.S3_layer.features)))

    def add_S4_features(self):
        print('Adding features for S4 layer')
        array_matrix_features = []  # Array of set features
        current_digit = -1
        max_comb_attrs = 4
        comb_attrs = 0
        for j in range(len(self.S3_layer.features)):

            if current_digit == -1:
                current_digit = self.S3_layer.features[j].digit
                for feature_matrix in self.S3_layer.features[j].matrix:
                    array_matrix_features.append(feature_matrix)
                comb_attrs += 1

            elif comb_attrs < max_comb_attrs and current_digit == self.S3_layer.features[j].digit:
                for feature_matrix in self.S3_layer.features[j].matrix:
                    array_matrix_features.append(feature_matrix)
                comb_attrs += 1

            else:
                comb_attrs = 0
                feature = ft.Feature(4, current_digit, array_matrix_features)
                self.S4_layer.add_last_feature(feature, 3)
                current_digit = self.S3_layer.features[j].digit
                array_matrix_features = []
                for feature_matrix in self.S3_layer.features[j].matrix:
                    array_matrix_features.append(feature_matrix)
                comb_attrs += 1



        print("Count features in S4 layer = " + str(len(self.S4_layer.last_layer_features)))

    def train_S1(self, picture):  # Inner picture 28*28
        self.S1_layer.learning_cells(picture)
        self.C1_layer.compute_outputs(self.S1_layer.array_SCell)

    def train_S2(self):
        self.S2_layer.learning_inner_cells(self.C1_layer.array_CCell)
        self.C2_layer.compute_outputs(self.S2_layer.array_SCell)

    def train_S3(self):
        self.S3_layer.learning_inner_cells(self.C2_layer.array_CCell)
        self.C3_layer.compute_outputs(self.S3_layer.array_SCell)

    def train_S4(self):
        self.S4_layer.learning_last_cells(self.C3_layer.array_CCell)
        self.C4_layer.compute_outputs(self.S4_layer.last_SCell)

    def train_first_layer(self, X_train, y_train):
        # for i in range(len(X_train)): # For each train picture, setting features and init weight
        #     print('Adding features of ' + str(i) + ' pictures.')
        #     picture = X_train[i]
        #     digit = np.argmax(y_train[i])
        #
        #     for j in range(16):
        #         matrix_area = fill_area(picture, j)
        #         if check_on_null(matrix_area): # Did not add null feature
        #             continue
        #
        #         feature = ft.Feature(1, digit, matrix_area)
        #
        #         if not self.S1_layer.has_feature(feature): # If feature is not in S1 layer -> add
        #             self.S1_layer.add_feature(feature, picture, 28)
        #
        # for i in range(len(X_train)): # For each train picture, learning cells
        #     print('Learning on ' + str(i) + ' pictures.')
        #     picture = X_train[i]
        #     self.S1_layer.learning_cells(picture)

        print("Count features in S1 layer = " + str(len(self.S1_layer.features)))

    def test(self, X_test, y_test):
        pass
