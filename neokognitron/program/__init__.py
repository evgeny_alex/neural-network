import gzip
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
import program.neokognitron as neo

def plot_image(pixels: np.array):
    plt.imshow(pixels.reshape((28, 28)), cmap='gray')
    plt.show()

def reading_labels():
    with gzip.open('train-labels-idx1-ubyte.gz') as train_labels:
        data_from_train_file = train_labels.read()

    # Пропускаем первые 8 байт
    label_data = data_from_train_file[8:]
    assert len(label_data) == 60000

    # Конвертируем каждый байт в целое число.
    # Это будет число от 0 до 9
    labels = [int(label_byte) for label_byte in label_data]
    assert min(labels) == 0 and max(labels) == 9
    assert len(labels) == 60000
    return labels

def reading_images():
    SIZE_OF_ONE_IMAGE = 28 ** 2
    images = []

    # Перебор тренировочного файла и читение одного изображения за раз
    with gzip.open('train-images-idx3-ubyte.gz') as train_images:
        train_images.read(4 * 4)
        ctr = 0
        for _ in range(60000):
            image = train_images.read(size=SIZE_OF_ONE_IMAGE)
            assert len(image) == SIZE_OF_ONE_IMAGE

            # Конвертировать в NumPy
            image_np = np.frombuffer(image, dtype='uint8') / 255
            images.append(image_np)

    return images

def encoding_labels(labels):
    labels_np = np.array(labels).reshape((-1, 1))

    encoder = OneHotEncoder(categories='auto')
    labels_np_onehot = encoder.fit_transform(labels_np).toarray()

    return labels_np_onehot

def precision(number, matrix):
    s = 0
    for index in range(10):
        s += matrix[number][index]
    if s == 0:
        return 0
    return matrix[number][number] / s

def recall(number, matrix):
    s = 0
    for index in range(10):
        s += matrix[index][number]
    if s == 0:
        return 0
    return matrix[number][number] / s

def f_measure(p, r):
    if p + r == 0:
        return 0
    return 2 * (p*r)/(p + r)

if __name__ == '__main__':
    labels = reading_labels()
    images = reading_images()
    labels_encoded = encoding_labels(labels)
    X_train, X_test, y_train, y_test = train_test_split(images, labels_encoded)
    # подготовили данные
    # plot_image(X_train[0])
    # print(X_train[0])
    # print(y_train[0])

    neokognitron = neo.Neokognitron()
    neokognitron.train(X_train, y_train)
    neokognitron.test(X_test, y_test)

    # confusion_matrix = [[2, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    #                     [0, 3, 0, 0, 1, 0, 0, 1, 0, 0],
    #                     [0, 0, 2, 0, 0, 1, 0, 0, 0, 0],
    #                     [0, 0, 1, 4, 0, 0, 0, 0, 1, 0],
    #                     [1, 0, 0, 0, 2, 0, 0, 0, 0, 0],
    #                     [0, 0, 0, 0, 0, 2, 0, 0, 1, 0],
    #                     [0, 0, 1, 0, 1, 0, 2, 0, 0, 0],
    #                     [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    #                     [0, 0, 0, 1, 0, 1, 0, 0, 3, 0],
    #                     [1, 0, 0, 0, 0, 0, 1, 0, 0, 2]]
    # accuracy = 0
    # n = 0
    # for i in range(10):
    #     for j in range(10):
    #         if i == j:
    #             accuracy += confusion_matrix[i][j]
    #         n += confusion_matrix[i][j]
    # for i in range(10):
    #     print(confusion_matrix[i])
    # print("count pictures = " + str(n))
    # print("accuracy = " + str(accuracy/n * 100) + " %")
    #
    # for i in range(10):
    #     p = precision(i, confusion_matrix)
    #     r = recall(i, confusion_matrix)
    #     f = f_measure(p, r)
    #     print(str(i) + ": precision = " + str(p) + ", recall = " + str(r) + ", F-мера = " + str(f))