import math

tau = 0.1           # Threshold of sensitivity
ALFA = 0.5
q = 100             # speed learnig

def fi(x):
    if x > 100:
        return 1
    if x < -50:
        return 0
    return 1 / (1 + math.exp((-1) * ALFA * x))

def wavelet(x):
    return (x*x - 1)*math.exp(-x*x/2)

class SCell:
    def __init__(self, features, weights, output, size, radius, size_weights):
        self.size_picture = size
        self.feautres = features
        self.weights = weights
        self.output_neurons = output
        self.radius_recp = radius  # radius receptive cell
        self.size_weights = size_weights

    def change_weights(self, picture):
        for i in range(len(self.output_neurons)):
            value = self.get_output_value(i, picture)
            self.output_neurons[i] = value

        max_index = self.output_neurons.index(max(self.output_neurons))
        self.change_weight_neuron(max_index, picture)

    def change_inner_weights(self, array_prev_CCells):
        for i in range(len(self.output_neurons)):
            value = self.get_inner_output_value(i, array_prev_CCells)
            self.output_neurons[i] = value

        max_index = self.output_neurons.index(max(self.output_neurons))
        self.change_inner_weight_neuron(max_index, array_prev_CCells)

    def change_weight_neuron(self, index, picture): # Change weight in neuron with max output
        weights = self.weights[index]
        col = index % self.size_picture
        row = int(index / self.size_picture)
        for j in range(-self.radius_recp, self.radius_recp):  # rows
            for k in range(-self.radius_recp, self.radius_recp):  # columns
                if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:  # Out the scope cell
                    # a.append(0)
                    continue
                else:
                    out_picture = picture[(row + j) * self.size_picture + col + k]
                    weights[(j + self.radius_recp) * 7 + (k + self.radius_recp)] += q * wavelet(out_picture) * out_picture

        self.weights[index] = weights

    def change_inner_weight_neuron(self, index, array_prev_CCells): # Change weight in neuron with max output
        if index >= len(self.weights) or len(self.weights) == 0:
            return
        count_of_features = len(self.weights[0])
        for cell in array_prev_CCells:
            picture = cell.output

            for i in range(count_of_features):  # То есть для каждого признака, суммируем для каждой плоскости

                weights = self.weights[index][i]

                col = index % self.size_picture
                row = int(index / self.size_picture)

                for j in range(-self.radius_recp, self.radius_recp):  # rows
                    for k in range(-self.radius_recp, self.radius_recp):  # columns
                        if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:  # Out the scope cell
                            # a.append(0)
                            continue
                        else:
                            out_picture = picture[(row + j) * (self.size_picture - 1) + col + k]
                            weights[(j + self.radius_recp) * (self.size_weights - 1) + (k + self.radius_recp)] += q * wavelet(out_picture) * out_picture

                self.weights[index][i] = weights

    # For inner cells
    def get_inner_output_value(self, index, array_prev_CCells):
        return (tau / (1 - tau)) * fi(((1 + self.check_inner_sum(index, array_prev_CCells)) /
                                       (1 + tau * self.inner_weight_b(index, array_prev_CCells) * self.inner_output_v(index, array_prev_CCells))) - 1)

    def check_inner_sum(self, index, array_prev_CCells):
        result = 0
        if index >= len(self.weights) or len(self.weights) == 0:
            return result

        count_of_features = len(self.weights[0])
        for cell in array_prev_CCells:
            picture = cell.output

            for i in range(count_of_features):  # То есть для каждого признака, суммируем для каждой плоскости

                col = index % self.size_picture
                row = int(index / self.size_picture)

                for j in range(-self.radius_recp, self.radius_recp):  # rows
                    for k in range(-self.radius_recp, self.radius_recp):  # columns
                        if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:
                            continue
                        else:
                            result += self.weights[index][i][j * (self.size_weights - 1) + k] * picture[
                                (row + j) * (self.size_picture-1) + col + k]

        return result

    def inner_weight_b(self, index, array_prev_CCells):  # Compute rule Hebba, weight b
        result_sum = 0
        if index >= len(self.weights) or len(self.weights) == 0:
            return result_sum
        count_of_features = len(self.weights[0])

        for cell in array_prev_CCells:
            picture = cell.output

            for i in range(count_of_features):  # То есть для каждого признака, суммируем для каждой плоскости

                col = index % self.size_picture
                row = int(index / self.size_picture)

                for j in range(-self.radius_recp, self.radius_recp):  # rows // +1
                    for k in range(-self.radius_recp, self.radius_recp):  # columns //+1
                        if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:
                            continue
                        else:
                            w = self.weights[index][i][j * (self.size_weights - 1) + k]
                            wavelet_value = wavelet(picture[(row + j) * (self.size_picture - 1) + col + k])
                            if wavelet_value == 0:
                                result_sum += 0
                            else:
                                result_sum += w * w / wavelet_value

        return math.sqrt(math.fabs(result_sum))

    def inner_output_v(self, index, array_prev_CCells):  # Output brake cell
        result_out = 0
        if index >= len(self.weights) or len(self.weights) == 0:
            return result_out

        count_of_features = len(self.weights[0])

        for cell in array_prev_CCells:
            picture = cell.output

            for i in range(count_of_features):  # То есть для каждого признака, суммируем для каждой плоскости

                col = index % self.size_picture
                row = int(index / self.size_picture)

                for j in range(-self.radius_recp, self.radius_recp):  # rows
                    for k in range(-self.radius_recp, self.radius_recp):  # columns
                        if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:
                            continue
                        else:
                            w = self.weights[index][i][j * self.size_weights + k]
                            result_out += w * w * wavelet(picture[(row + j) * self.size_picture + col + k])

        return math.sqrt(math.fabs(result_out))

    # For first cells
    def get_output_value(self, index, picture):
        return (tau/(1 - tau))*fi(((1 + self.check_sum(index, picture)) /
                                  (1 + tau * self.weight_b(index, picture) * self.output_v(index, picture))) - 1)

    def check_sum(self, index, picture):
        result = 0
        col = index % self.size_picture
        row = int(index / self.size_picture)
        for j in range(-self.radius_recp, self.radius_recp): # rows
            for k in range(-self.radius_recp, self.radius_recp): # columns
                if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:
                    continue
                else:
                    result += self.weights[index][j * self.size_weights + k] * picture[(row + j) * (self.size_picture - 1) + col + k]

        return result

    def weight_b(self, index, picture): # Compute rule Hebba, weight b
        result_sum = 0
        col = index % self.size_picture
        row = int(index / self.size_picture)
        for j in range(-self.radius_recp, self.radius_recp):  # rows // +1
            for k in range(-self.radius_recp, self.radius_recp):  # columns //+1
                if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:
                    continue
                else:
                    w = self.weights[index][j * self.size_weights + k]
                    wavelet_value = wavelet(picture[(row + j) * (self.size_picture - 1) + col + k])
                    if wavelet_value == 0:
                        result_sum += 0
                    else:
                        result_sum += w * w / wavelet_value

        return math.sqrt(math.fabs(result_sum))

    def output_v(self, index, picture): # Output brake cell
        result_out = 0
        col = index % self.size_picture
        row = int(index / self.size_picture)
        for j in range(-self.radius_recp, self.radius_recp):  # rows
            for k in range(-self.radius_recp, self.radius_recp):  # columns
                if col + k < 0 or col + k > self.size_picture - 1 or row + j < 0 or row + j > self.size_picture - 1:
                    continue
                else:
                    w = self.weights[index][j * self.size_weights + k]
                    result_out += w * w * wavelet(picture[(row + j) * self.size_picture + col + k])

        return math.sqrt(math.fabs(result_out))