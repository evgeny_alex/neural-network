class Feature:
    def __init__(self, layer, digit, array):
        self.number_layer = layer
        self.digit = digit
        self.matrix = array

    def __eq__(self, other):
        if isinstance(other, Feature):
            return (self.digit == other.digit and self.matrix == other.matrix
                    and self.number_layer == other.number_layer)
        return NotImplemented