import math

class Neuron:
    SPEED_LEARNING = 0.05
    ALFA = 1

    def __init__(self, name):
        self.attrs_name = name
        self.flag_neuron_first_layer = False
        self.flag_neuron_output = False
        self.previous_neurons = []
        self.previous_weight = []
        self.input_value = 0
        self.error = 0
        self.local_gradient = 0
        self.value = 0

    def get_output_value(self):
        if self.flag_neuron_first_layer:
            if math.isnan(self.input_value):
                self.value = 0
            else:
                self.value = self.fi(self.previous_weight[0]*self.input_value)
        else:
            self.value = self.fi(self.v())
        return self.value

    def get_value(self):
        return self.value

    def fi(self, v_j):
        if v_j > 100:
            return 1
        if v_j < -50:
            return 0
        return 1 / (1 + math.exp((-1) * self.ALFA * v_j))

    def v(self):  # без порога b
        result = 0
        for i in range(len(self.previous_weight)):
            result += float(self.previous_weight[i]) * float(self.previous_neurons[i].get_output_value())
        return result

    def set_prev_neurons(self, list_neurons):
        self.previous_neurons = list_neurons

    def set_prev_weight(self, w):
        self.previous_weight = w

    def set_error(self, e):
        self.error = e

    def set_local_gradient(self): # устанавливаем локальный градиент для выходного слоя
        self.local_gradient = self.error * self.value * (1 - self.value) * self.ALFA

    def set_local_gradient_for_hidden_layers(self, sum_grad):
        self.local_gradient = sum_grad * self.value * (1 - self.value) * self.ALFA

    def change_weight(self):
        if self.flag_neuron_first_layer:
            self.previous_weight[0] -= self.SPEED_LEARNING * self.local_gradient * self.input_value
        else:
            for i in range(len(self.previous_neurons)):
                self.previous_weight[i] -= self.SPEED_LEARNING * self.local_gradient * self.previous_neurons[i].get_value()

    def get_multiply_grad(self, i):
        return self.local_gradient * self.previous_weight[i]

    def change_input_value(self, v):
        self.input_value = v