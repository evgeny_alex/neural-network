import pandas as pd
import program.neuronnet as nrn

def reading_data():
    a = pd.ExcelFile('data_set.xlsx')
    df = a.parse(a.sheet_names[1], skiprows=0, index_col=None, na_values=['None'])
    return df

if __name__ == '__main__':
    df = reading_data()
    net = nrn.NeuronNet(df)
    net.start_learning()
    net.testing()