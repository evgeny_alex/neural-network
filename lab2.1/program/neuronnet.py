import math
import program.neuron as neuron
import random
import matplotlib.pyplot as plt

init_min_weight = 0.5
init_max_weight = -0.5

len_learning_set = 65

def show_diagram(attr, array, name):
    fig, ax = plt.subplots()
    ax.bar(attr, array)
    ax.set_title(name)
    ax.set_facecolor('seashell')
    ax.set_facecolor('floralwhite')
    fig.set_figwidth(12)  # ширина Figure
    fig.set_figheight(6)  # высота Figure

    plt.show()

class NeuronNet:
    df = 0
    important_attrs = []
    neurons_first_layer = []  # нейроны первого слоя
    neurons_second_layer = []  # нейроны второго слоя
    output_neurons = []  # выходные нейроны
    E = 0 # ошибка сети

    def __init__(self, data):
        self.df = data
        self.drop_attrs()
        self.important_attrs = list(self.df.columns.array)
        self.important_attrs.remove('G_total')
        self.important_attrs.remove('КГФ')
        self.d_kgf = {}
        self.d_G_total = {}
        self.list_norm_attr = []
        self.list_avg_E = []
        self.avg_E = 0
        self.normalization_main_attr()
        self.normalization_important_attr()
        self.accuracy_kgf = 0
        self.accuracy_G_total = 0

        self.init_weight()

    def init_weight(self):
        self.init_weight_first_layer()
        self.init_weight_second_layer()
        self.init_weight_last_layer()

    def init_weight_first_layer(self):
        # list_unique_values = []
        # for attr in self.important_attrs:
        #     list_unique_values_current_attr = list(self.df[attr].value_counts().axes[0])
        #     list_unique_values.append(list_unique_values_current_attr)
        # for i in range(len(list_unique_values)):
        #     for j in range(len(list_unique_values[i])):
        #         n = neuron.Neuron(self.important_attrs[i])
        #         n.flag_neuron_first_layer = True
        #         self.neurons_first_layer.append(n)
        for i in range(60):
            n = neuron.Neuron('attr')
            n.flag_neuron_first_layer = True
            self.neurons_first_layer.append(n)


    def init_weight_second_layer(self):
        for i in range(len(self.df) + 30):
            self.neurons_second_layer.append(neuron.Neuron('set'))
        for i in range(len(self.neurons_second_layer)):
            weights = []
            for j in range(len(self.neurons_first_layer)):
                weights.append(random.uniform(init_min_weight, init_max_weight))
            self.neurons_second_layer[i].set_prev_neurons(self.neurons_first_layer)
            self.neurons_second_layer[i].set_prev_weight(weights)

    def init_weight_last_layer(self):
        n1 = neuron.Neuron('set')
        n2 = neuron.Neuron('set')
        n1.flag_neuron_output = True
        n2.flag_neuron_output = True
        self.output_neurons.append(n1)
        self.output_neurons.append(n2)
        for n in self.output_neurons:
            weights = []
            for i in range(len(self.neurons_second_layer)):
                weights.append(random.uniform(init_min_weight, init_max_weight))
            n.set_prev_neurons(self.neurons_second_layer)
            n.set_prev_weight(weights)

    def normalization_important_attr(self):
        for attr in self.important_attrs:
            list_unique_values = list(self.df[attr].value_counts().axes[0])
            list_unique_values.sort()
            len_list = len(list_unique_values)
            d_list = {}
            for i in range(len_list):
                d_list[list_unique_values[i]] = (list_unique_values[i] - list_unique_values[0]) / (
                            list_unique_values[len_list - 1] - list_unique_values[0])
            self.list_norm_attr.append(d_list)

    def normalization_main_attr(self):
        list_unique_values_kgf = list(self.df['КГФ'].value_counts().axes[0])
        list_unique_values_G_total = list(self.df['G_total'].value_counts().axes[0])
        list_unique_values_kgf.sort()
        list_unique_values_G_total.sort()
        len_list_kgf = len(list_unique_values_kgf)
        len_list_G_total = len(list_unique_values_G_total)
        for i in range(len_list_kgf):
            self.d_kgf[list_unique_values_kgf[i]] = (list_unique_values_kgf[i] - list_unique_values_kgf[0])/(list_unique_values_kgf[len_list_kgf - 1] - list_unique_values_kgf[0])
        for i in range(len_list_G_total):
            self.d_G_total[list_unique_values_G_total[i]] = (list_unique_values_G_total[i] - list_unique_values_G_total[0])/(list_unique_values_G_total[len_list_G_total - 1] - list_unique_values_G_total[0])

    def denormalize_kgf(self, value):
        list_unique_values_kgf = list(self.df['КГФ'].value_counts().axes[0])
        list_unique_values_kgf.sort()
        len_list_kgf = len(list_unique_values_kgf)
        sub = list_unique_values_kgf[len_list_kgf - 1] - list_unique_values_kgf[0]
        result = list_unique_values_kgf[0] + value * sub

        return result

    def denormalize_G_total(self, value):
        list_unique_values_G_total = list(self.df['G_total'].value_counts().axes[0])
        list_unique_values_G_total.sort()
        len_list_G_total = len(list_unique_values_G_total)
        sub = list_unique_values_G_total[len_list_G_total - 1] - list_unique_values_G_total[0]
        result = list_unique_values_G_total[0] + value * sub
        return result

    def testing(self):
        len_test_set = len(self.df) - len_learning_set
        success_answer_kgf = 0
        success_answer_G_total = 0
        # for i in range(len_test_set):
        #     index = i + len_learning_set
        for index in range(18, 33):
            self.set_input(1, index)
            test_value_kgf = self.denormalize_kgf(self.output_neurons[0].get_output_value())
            test_value_G_total = self.denormalize_G_total(self.output_neurons[1].get_output_value())
            if self.check_answer_kgf(index, test_value_kgf):
                success_answer_kgf += 1
            if self.check_answer_G_total(index, test_value_G_total):
                success_answer_G_total += 1

        self.accuracy_kgf = (success_answer_kgf / 15) * 100
        self.accuracy_G_total = (success_answer_G_total / 15) * 100
        print('Accuracy KGF = ' + str(self.accuracy_kgf) + ' %.')
        print('Accuracy G_total = ' + str(self.accuracy_G_total) + ' %.')

    def check_answer_G_total(self, index, value):
        expected_value = self.df['G_total'][index]
        if math.isnan(expected_value):
            return True
        if math.fabs(expected_value - value) < 2:
            return True
        else:
            return False

    def check_answer_kgf(self, index, value):
        expected_value = self.df['КГФ'][index]
        if math.fabs(expected_value - value) < 30:
            return True
        else:
            return False

    def start_learning(self):
        list_epoch = []
        for j in range(4):
            print('Number of epoch - ' + str(j))
            self.avg_E = 0
            self.learning_epoch(j)
            list_epoch.append(j)
            print('E_avg = ' + str(self.avg_E) + ' in ' + str(j) + ' epoch.')
        print(self.list_avg_E)
        show_diagram(list_epoch, self.list_avg_E, "Средняя энергия ошибки")

    def learning_epoch(self, j):
        for i in range(18, 33):
            print(str(i) + ' row of learning.')
            self.set_input(j, i)
            output_kgf = self.output_neurons[0].get_output_value()
            output_G_total = self.output_neurons[1].get_output_value()

            d_kgf = self.d_kgf[self.df['КГФ'][i]]
            if math.isnan(self.df['G_total'][i]):
                continue
            d_G_total = self.d_G_total[self.df['G_total'][i]]

            error_kgf = output_kgf - d_kgf
            error_G_total = output_G_total - d_G_total

            self.E = (pow(error_G_total, 2) + pow(error_kgf, 2)) / 2
            self.avg_E += self.E

            self.output_neurons[0].set_error(error_kgf)
            self.output_neurons[1].set_error(error_G_total)

            self.set_local_gradient()   # устанавливаем локальные градиенты в нейроны
            self.change_weight()        # меняем веса между нейронами
        self.avg_E = self.avg_E / len(self.df)
        self.list_avg_E.append(self.avg_E)

    def set_local_gradient(self):
        self.output_neurons[0].set_local_gradient()
        self.output_neurons[1].set_local_gradient()

        for i in range(len(self.neurons_second_layer)):
            grad_weight_kgf = self.output_neurons[0].get_multiply_grad(i)
            grad_weight_G_total = self.output_neurons[1].get_multiply_grad(i)
            self.neurons_second_layer[i].set_local_gradient_for_hidden_layers(grad_weight_G_total + grad_weight_kgf)

        for j in range(len(self.neurons_first_layer)):
            sum_grad = 0
            for k in range(len(self.neurons_second_layer)):
                sum_grad += self.neurons_second_layer[k].get_multiply_grad(j)
            self.neurons_first_layer[j].set_local_gradient_for_hidden_layers(sum_grad)

    def change_weight(self):
        for i in range(len(self.neurons_first_layer)):
            self.neurons_first_layer[i].change_weight()
        for j in range(len(self.neurons_second_layer)):
            self.neurons_second_layer[j].change_weight()
        self.output_neurons[0].change_weight()
        self.output_neurons[1].change_weight()

    def setting_inputs_for_first_layer(self, attr, value, iteration_i, iteration_j):
        for i in range(len(self.neurons_first_layer)):
            n = self.neurons_first_layer[i]
            # if n.attrs_name == attr:
            #     n.change_input_value(value)
            #     if iteration_i == 0 and iteration_j == 18:
            #         n.previous_weight.append(random.uniform(init_min_weight, init_max_weight))
            if iteration_i == 0 and iteration_j == 18:
                n.previous_weight.append(random.uniform(init_min_weight, init_max_weight))

    def set_input(self, i, j):
        for index in range(len(self.important_attrs)):
            attr = self.important_attrs[index]
            norm_value = self.list_norm_attr[index][self.df[attr][i]]
            self.setting_inputs_for_first_layer(attr, norm_value, i, j)  # установили входные значения

    def drop_attrs(self):
        self.df = self.df.drop(['Dшт'], axis=1)
        self.df = self.df.drop(['Ro_w'], axis=1)
        self.df = self.df.drop(['Ro_c'], axis=1)
        self.df = self.df.drop(['Ro_g'], axis=1)
        self.df = self.df.drop(['Рпл. Тек (Карноухов)'], axis=1)
        self.df = self.df.drop(['Тна шлейфе'], axis=1)
        self.df = self.df.drop(['Нэф'], axis=1)
        self.df = self.df.drop(['Удельная плотность газа '], axis=1)
        self.df = self.df.drop(['Руст1'], axis=1)
        self.df = self.df.drop(['Рзаб1'], axis=1)
        self.df = self.df.drop(['Рзаб'], axis=1)
        self.df = self.df.drop(['Дебит газа'], axis=1)
        self.df = self.df.drop(['Дебит воды1'], axis=1)
        self.df = self.df.drop(['Рлин1'], axis=1)