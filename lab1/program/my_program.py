import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math


def missing_values(df):
    attr = list(df)
    sum_isna = df.isna().sum()
    show_diagram(attr, sum_isna, 'Пропущенные значения')

def unique_values(df):
    attr = list(df)
    unique_array = []
    for str in attr:
        unique_array.append(len(df[str].value_counts()))

    show_diagram(attr, unique_array, 'Уникальные значения')

def show_diagram(attr, array, name):
    fig, ax = plt.subplots()
    ax.barh(attr, array)
    ax.set_title(name)
    ax.set_facecolor('seashell')
    ax.set_facecolor('floralwhite')
    fig.set_figwidth(12)  # ширина Figure
    fig.set_figheight(6)  # высота Figure

    plt.show()

def corr_attr(df):
    plt.figure(figsize=(20,20))
    sns.heatmap(df.corr(), annot=True, fmt='.0%')
    plt.show()

def important_diagram(df):
    attr = list(df)
    attr.remove('КГФ')
    attr.remove('G_total')
    gain_ratio_array = []

    for str in attr:
        print(str)
        gain = gain_ratio(str, df)
        print(gain)
        gain_ratio_array.append(gain)

    show_diagram(attr, gain_ratio_array, 'Диаграмма важности признаков')

def gain_ratio(current_attr, df):
    info_gain = 0
    split_info = 0
    info_gain += entropy('attr','without', df)
    value_attr = df[current_attr].value_counts()
    df[current_attr].value_counts()
    N = len(df)
    i = 0
    l = value_attr.axes[0].array
    for attr in value_attr:
        k = attr/N
        value = l[i]
        info_gain += k * entropy(current_attr, value, df)
        split_info += k * math.log(k)

    return info_gain/math.fabs(split_info)

def entropy(attr, value, df):
    S = 0
    list_unique_index = get_list_unique_index(df)

    for index in list_unique_index:
        if attr == 'attr':
            N = len(df)
        else:
            N = get_count_rows(df, attr, value, -1)
        Nj = get_count_rows(df, attr, value, index)
        k = Nj/N
        if k == 0:
            continue
        S -= k * math.log(k) / math.log(2)

    return S

def get_list_unique_index(df):
    value_attr = []
    list_G_total = df['G_total']
    list_KGF = df['КГФ']
    list_unique_index = []
    i = 0
    while i < len(list_G_total):
        item = str([list_G_total[i], list_KGF[i]])
        i += 1
        if item not in value_attr:
            list_unique_index.append(i)
            value_attr.append(item)
    return list_unique_index

def get_count_rows(df, attr, value, index):
    attr_G_total = 0
    attr_KGF = 0
    if index != -1:
        attr_G_total = df['G_total'][index]
        attr_KGF = df['КГФ'][index]
    count = 0
    for i in range(len(df)):
        if index == -1 or df['G_total'][i] == attr_G_total and df['КГФ'][i] == attr_KGF:
            if attr == 'attr':
                count += 1
            elif df[attr][i] == value:
                count += 1
    return count

def reading_data():
    a = pd.ExcelFile('data_set.xlsx')
    df = a.parse(a.sheet_names[1], skiprows=0, index_col=None, na_values=['None'])
    return df

if __name__ == '__main__':
    df = reading_data()
    missing_values(df) # Пропущенные значения
    unique_values(df) # Уникальные значения
    corr_attr(df) # Корреляция атрибутов
    # important_diagram(df) # Диаграмма важности признаков