package main.java;

import java.util.Map;
import java.util.TreeMap;

public class DecisionTree {
    private String attr;
    private Map<String, DecisionTree> node = new TreeMap<>(); // в map будет храниться значение атрибута и ссылка на след ноду

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public void addToTree(String value, DecisionTree newTree) {
        node.put(value, newTree);
    }
}
