package main.java;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Algorithm {
    private String mainAttr = "5Play";

    public static void main(String[] args) throws IOException {
        Algorithm algorithm = new Algorithm();
        Table table = new Table("test.csv");
        Map<String, String> currentWay = new TreeMap<>(); // текущий выбор атрибутов
        DecisionTree tree = algorithm.makeTree(table, currentWay);
        System.exit(0);
    }

    private DecisionTree makeTree(Table table, Map<String, String> currentWay) {
        DecisionTree tree = new DecisionTree();

        float maxInfoGain = 0;
        String resAttr = "";
        String attr;

        for (int i = 0; i < table.getCountAttr(); i++) { // выбираем атрибут с максимальным infoGain
            attr = table.getAttr(i);
            if (attr.equals(mainAttr)) continue;
            float IG = infoGainRatio(currentWay, attr, table);
            if (maxInfoGain < IG) {
                maxInfoGain = IG;
                resAttr = attr;
            }
        }
        if (resAttr.equals("")) return tree;
        tree.setAttr(resAttr);
        List<String> valueAttr = table.getValueList(resAttr); // получили лист различных значений атрибута

        for (String value : valueAttr) { // для каждого атрибута создаем дерево
            Map<String, String> newWay = new TreeMap<>();
            newWay.putAll(currentWay);
            newWay.put(resAttr, value);
            DecisionTree newTree = makeTree(table, newWay);
            tree.addToTree(value, newTree);
        }

        return tree;
    }

    private float infoGainRatio(Map<String, String> currentWay, String attr, Table table) {
        float IG = 0;
        float splitInfo = 0;
        IG += entropy(currentWay, table);
        List<String> valueAttr = table.getValueList(attr); // получили лист различных значений атрибута
        for (String s : valueAttr) {
            Map<String, String> newWay = new TreeMap<>(); // составляем новый путь
            newWay.putAll(currentWay);
            newWay.put(attr, s);
            float N = table.getCountWithWay(currentWay);
            float Nj = table.getCountWithWay(newWay);
            float k = Nj / N;
            IG -= (k) * entropy(newWay, table);
            splitInfo += k * Math.log(k);
        }
        return IG/Math.abs(splitInfo);
    }

    private float entropy(Map<String, String> currentWay, Table table) {
        float S = 0;
        List<String> valueAttr = table.getValueList(mainAttr); // получили лист различных значений атрибута
        for (String str : valueAttr) {
            Map<String, String> newWay = new TreeMap<>(); // составляем новый путь
            newWay.putAll(currentWay);
            newWay.put(mainAttr, str);
            float N = table.getCountWithWay(currentWay);
            float Nj = table.getCountWithWay(newWay);
            float k = Nj / N;
            if (k == 0) continue;
            S -= k * Math.log(k) / Math.log(2);
        }
        return S;
    }
}
