package main.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Table {
    Map<String, ArrayList<String>> table = new TreeMap<>();

    public Table(String path) throws IOException { // конструктор, который парсит таблицу из файла
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String line;
        Scanner scanner;

        line = reader.readLine();
        scanner = new Scanner(line);
        scanner.useDelimiter(",");
        Integer index = 1;
        while (scanner.hasNext()) { // Заполняем атрибуты
            String data = scanner.next();
            table.put(index.toString() + data, new ArrayList<>());
            index++;
        }

        while ((line = reader.readLine()) != null) {
            scanner = new Scanner(line);
            scanner.useDelimiter(",");
            Iterator<Map.Entry<String, ArrayList<String>>> entries = table.entrySet().iterator();
            while (scanner.hasNext()) { // Заполняем атрибуты
                String s = scanner.next();
                Map.Entry<String, ArrayList<String>> entry = entries.next();
                ArrayList<String> list = entry.getValue();
                list.add(s);
                table.put(entry.getKey(), list);
            }
        }
    }

    public int getCountAttr() {
        return table.size();
    }

    public String getAttr(int index) {
        int i = 0;
        Iterator<Map.Entry<String, ArrayList<String>>> entries = table.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, ArrayList<String>> entry = entries.next();
            if (i == index) return entry.getKey();
            i++;
        }
        return "";
    }

    public List<String> getValueList(String attr) {
        List<String> list = table.get(attr);    // получили общий лист значений
        Map<String, Integer> result = new TreeMap<>();  // в результате вернем только эксклюзивные
        List<String> listResult = new ArrayList<>();
        for (String s : list) {
            result.put(s, 0);
        }
        for (Map.Entry<String, Integer> entry : result.entrySet()) {
            listResult.add(entry.getKey());
        }
        return listResult;
    }

    public int getCountWithWay(Map<String, String> currentWay) {
        int count = 0;
        if (currentWay == null) return table.get("5Play").size();
        for (int i = 0; i < table.get("5Play").size(); i++) {
            boolean checkLine = true;
            for (Map.Entry<String, String> e : currentWay.entrySet()) {
                String attr = e.getKey();
                if (!table.get(attr).get(i).equals(e.getValue())) checkLine = false;
            }
            if (checkLine) count++;
        }
        return count;
    }
}
